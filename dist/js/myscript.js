$(document).ready(function(){
    // showRadioData();
    // showRadio("104.9");
    showNumTotalStreamings();
    showNumUsers();
    showNumOnlineUsers();
});

var map = null;

var markers = [];
function initMap() {
    var myLatLng = {lat: 19.430119, lng: -99.138867};

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 9,
        center: myLatLng
    });
    var pinImageActive = {
        url: "http://maps.google.com/mapfiles/kml/pal3/icon46.png", // url
        scaledSize: new google.maps.Size(50, 50), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
    };

    var usersRef = new Firebase('https://fmobile.firebaseio.com/Usuarios');
    usersRef.orderByChild("status").equalTo("online").on("value", function(snapshot) {
        for (m in markers) {
            markers[m].setMap(null);
        }
        markers = [];
        snapshot.forEach(function(childSnapshot){
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(childSnapshot.val().location['lat'], childSnapshot.val().location['lon']),
                title: childSnapshot.val().name,
                icon: pinImageActive
            });
            markers.push(marker);
        });

        var markerCluster = new MarkerClusterer(map, markers);
    });
}
function showNumTotalStreamings(){
    var radioRef = new Firebase('https://fmobile.firebaseio.com/Radio');
    radioRef.on("value", function(snapshot) {
        var a = snapshot.numChildren();
        $('#num-history').text(a);

    });
}

function showNumUsers(){
    var radioRef = new Firebase('https://fmobile.firebaseio.com/Usuarios');
    radioRef.on("value", function(snapshot) {
        var a = snapshot.numChildren();
        $('#num-users').text(a);

    });
}
function showNumOnlineUsers(){
    var radioRef = new Firebase('https://fmobile.firebaseio.com/Usuarios');
    radioRef.orderByChild("status").equalTo("online").on("value", function(snapshot) {
        var a = snapshot.numChildren();
        $('#num-online-users').text(a);
    });
}
function showRadioData(){
    var radioRef = new Firebase('https://fmobile.firebaseio.com/Radio');
    var content = "";
    radioRef.limitToLast(10).on("value", function(snapshot) {
    content = "<table class='table table-hover'>"
        + "<tr>"
        + "<th aling='center'>user_id</th>"
        + "<th aling='center'>station_frec</th>"
        + "<th aling='center'>station_id</th>"
        + "<th aling='center'>init_time</th>"
        + "<th aling='center'>final_time</th>"
        + "</tr>";
      snapshot.forEach(function(childSnapshot) {
            var childData = childSnapshot.val();
            content += '<tr>'
                + '<td>' + childData.user_id + '</td>'
                + '<td>' + childData.station_frec + '</td>'
                + '<td>' + childData.station_id + '</td>'
                + '<td>' + childData.init_time + '</td>'
                + '<td>' + childData.final_time + '</td>'
                + '</tr>';
          });
            content += "</table>";

            $('#tbradio').html(content);

    });
}
function showRadio(emisora){
    var radioRef = new Firebase('https://fmobile.firebaseio.com/Radio');
    var content = "";
    radioRef.on("value", function(snapshot) {
    content = "<table class='table table-hover'>"
        + "<tr>"
        + "<th aling='center'>user_id</th>"
        + "<th aling='center'>station_frec</th>"
        + "<th aling='center'>station_id</th>"
        + "<th aling='center'>init_time</th>"
        + "<th aling='center'>final_time</th>"
        + "</tr>";
      snapshot.forEach(function(childSnapshot) {
            var childData = childSnapshot.val();
            if (childData.station_frec == emisora){
                content += '<tr>'
                + '<td>' + childData.user_id + '</td>'
                + '<td>' + childData.station_frec + '</td>'
                + '<td>' + childData.station_id + '</td>'
                + '<td>' + childData.init_time + '</td>'
                + '<td>' + childData.final_time + '</td>'
                + '</tr>';
            }
            
          });
            content += "</table>";
            var sss = '#104';
            $(sss).html(content);

    });
}
